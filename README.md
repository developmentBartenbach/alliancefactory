# PROJECT NAME #

[Write a nice sentence about your hot, new project here.]


## How to use this boilerplate

To get a new project set up based on this project boilerplate, simply follow these steps:

```bash
git clone git@bitbucket.org:connecta-ag/cag_t3boilerplate.git mynewproject_www
cd mynewproject_www
```
Change values of next variables in Makefile according to you needs:

- TYPO3_SITE_NAME
- TYPO3_SITE_DOMAIN (**!don't forget to add domain to the hosts file on your computer**)
- COMPOSE_PROJECT_NAME
- HTTP_PORT
- HTTPS_PORT
- MYSQL_PORT
- MYSQL_HOST
- MYSQL_ROOT_PASSWORD
- MYSQL_USER
- MYSQL_PASSWORD
- MYSQL_DATABASE
- MYSQL_DUMP (you can put into _temp folder dump.sql of your project and change MYSQL_DUMP variable value to use own database)

And run:

```bash
$ make init-project-and-git
```

## How do I get set up? ###

This project ships with a fully fledged docker environment. Please read this README carefully, if you want to make use
of this feature and use the shipped TYPO3 environment.

If you don't need the full T3 installation and you would like to stick to plain html-file based frontend development,
just check the following quick start instructions.

### Quick start for frontend development ###

* Make sure, you have PHP (7.2+) and composer installed.
* Enter local GIT repo directory
* run `make init-dev` to install composer + npm components
* run `npm run server` to start the webpack dev server
* and now... enjoy coding! :)

#### Requirements

* PHP 7.2+ (Web & CLI)
* docker
* docker-compose
* bash

*Note: Docker installation mostly only needed for backend developers or if you want a fully automated installaton
of the T3 website.

### Local Setup with TYPO3 docker containers and database ###

To install and start running TYPO3 locally please run next command from project root directory:

```bash
$ make install
```

By default development enviroment will be configured, to run TYPO3 in production (staging) mode please run:

```bash
$ make install MODE=prod
```
or

```bash
$ make install MODE=stage
```

To see all available commands in Makefile please run:

```bash
$ make help
```

### Use .env Password encryption ###

For the encryption to be used, a private key must be created.
```
./vendor/bin/generate-defuse-key >> env_secret.txt
```
The secret stored in the file is now used to decrypt the passwords. To do this, the file must be referenced with the secret in the .env file. The corresponding entry is:
```
ENV_CRYPTO_FILE_PATH="env_secret.txt"
```

To encrypt a password, the following Robo task can be used:
```
./vendor/consolidation/robo/robo t3:crypto-encrypt-value <your_password_here>
```
By default, only the TYPO3 password is decrypted in the file "public / typo3conf / AdditionalConfiguration.php". If other passwords are to be decrypted, this must be extended accordingly.


### Add / develop project-specific extensions ###

Since version 3.6.0, project-specific extensions (including `EXT:tmpl`) will no longer be edited within the folder
`typo3conf/ext` but rather in `src/my-very-specific-ext`. Due to the newly added _path_ repository in `composer.json`,
the extensions from `src/` will then undergo the same installation process as any other official TER extension.

Hence, exception rules in `.gitignore` and the `psr-4` setup in `composer.json` will no longer be necessary. The directory
`typo3conf/ext/` is a directory now only manipulated by install scripts (mainly `composer install` and `update`).

### Basic Template Extension / Frontend Build Toolchain ###

All resources of **frontend toolchain**  (see `package.json`, `build/frontend`) usually are stored in `EXT:tmpl` (scss, js sources, fonts, icons, etc.). make install should do most of the job to get set up initially.

**Run all npm commands from inside docker container.** For example, to see all available FE build tasks please run:

```bash
$ make ssh
```
or

```bash
$ make root-ssh
```

This will ssh into docker application container and then:

```bash
$ npm run
```

For JS- and CSS-processing, please use `npm run prod` (production) or `npm run dev`
(development with sourcemaps, etc.) or the respective watchers.

#### npm run command overview ####

Basically, these are the most important _build_ commands:

* `npm run prod` - build CSS and JS in **production** mode (minified / uglified)
* `npm run dev` - build CSS and JS in **development** mode (with CSS source map)

Using _webpack-dev-server_ for development (for now, only static HTML recommended):

* `npm run server` – starts dev server http://localhost:8080 with watch and live reload feature
* `npm run sever-remote` starts dev server http://localhost:8080 and uses [ngrok](https://www.npmjs.com/package/ngrok)
  to establish a remote tunnel to your local dev environment

Check `npm run` and **package.json** for further commands and more details.

## Code Quality Tools ##

To ensure consistent and streamlined code, the following tools / hints are to be used and respected:

* `.editorconfig` must be respected by your editor of choice
* Your PHP IDE must use PSR-2 as a coding style standard
* PHP CodeSniffer / PHP CS Fixer
* TypoScript Linting
* SCSS Linting


### PHPStan - Static Code Analysis ###

Please run PHPStan on all self-developed extensions to ensure high code quality and find possible errors
or important warnings or use of deprecated code as early as possible in the development process.

You run it via composer script:

`composer analyze:php`

Or run it natively (be sure to add the fitting arguments):

`./vendor/bin/phpstan analyse --ansi --no-progress --configuration=phpstan.neon --level=7 public/typo3conf/ext/`

Since TYPO3 contains some strange code usages which cannot be understood easily by a static code analyzer, we can
define exceptions and configure PHPStan to out needs using the PHPStan config file:

`./phpstan.neon`

For more information on how to use / configure PHPStan, check these websites:

* https://github.com/phpstan/phpstan
* https://medium.com/@ondrejmirtes/phpstan-2939cd0ad0e3
* https://phpstan.org (only version to just paste in code and have it checked)


### PHP CS Fixer ###

See https://github.com/FriendsOfPHP/PHP-CS-Fixer

The PHP Coding Standards Fixer (PHP CS Fixer) tool fixes your code to follow standards. In our case, the PSR-2 based
standards extended / edited by the TYPO3 CMS Core team. See file `.php_cs`. The tool is installed via composer's dev
dependencies.

It is advisable to also install it globally on your local development machine.

Execute dry-run, listing files which would be touched:

`./vendor/bin/php-cs-fixer fix --dry-run`

Execute dry-run, listing files which would be touched, plus diff:

`./vendor/bin/php-cs-fixer fix --dry-run --diff`

Now, executing the fixing process for real would be plain and simple the following command:

`./vendor/bin/php-cs-fixer fix`

The file `.php_cs` contains the coding style rules as well as all directories and files
to check and to ignore (see `$finder`).

You can also override the configured directories and files by adding a specific path / file to check, eg.:

`./vendor/bin/php-cs-fixer fix --dry-run public/typo3conf/ext/my_ext/my_path_or_file`

**Advice:** Check your IDE's features. Usually, IDEs like **phpStorm** make it easy to include those tools.

**! Notice:** PHP CS Fixer is called before every `git commit ...` command, to skip checking PHP code run `git commit ... --no-verify` (not recommended).

### PHP Code Sniffer ###

in composer.json "squizlabs/php_codesniffer": "3.4.*",

**see:**

* https://github.com/squizlabs/PHP_CodeSniffer (Readme)
* https://scrutinizer-ci.com/docs/tools/php/code-sniffer/
* https://www.thewebhatesme.com/entwicklung/php-code-sniffer-installation-und-verwendung/
* https://github.com/squizlabs/PHP_CodeSniffer/wiki/Reporting
* https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage
* https://github.com/cakephp/cakephp-codesniffer/issues/137


**Configuration:**

File in Root: **.phpcs.xml** (set to PSR2 code formatting)
(copied und enhanced from ./vendor/squizlabs/php_codesniffer/src/Standards/PSR2/ruleset.xml)


#### Usage over shell:

**Analyse**

`./vendor/bin/phpcs -s --report-summary=./_temp/phpcs-summary.txt --report-code=./_temp/phpcs-code.txt --extensions=php,inc,lib  ./src`

**Options**
 * `-s` - print out sniffer name by warnings ans errors
 * `--report-summary=./_temp/phpcs-summary.txt` - summary report and save to path
 * `--report-code=./_temp/phpcs-code.txt` - report with detailed information for messages and save to path
 * `--extensions=php,inc,lib` - file types to scan
 * `./src` - last parameter ist the whole target folder or file path

**Fix Errors automatically**

Some errors can be fixed automatically. The following command can be used for this

`./vendor/bin/phpcbf --extensions=php,inc,lib ./src/`

#### Usage over PhpStorm:

**see:**

* https://www.jetbrains.com/help/phpstorm/using-php-code-sniffer.html (PhpStorm)

First edit Preferences In PhpStorm (cmd ,)
* "Preferences | Languages & Frameworks | PHP | Quality Tools" : Select Code Sniffer and map local path "./vendor/bin/phpcs" and "./vendor/bin/phpcbf" for PHP Code Beautifier and Fixer Settings
* "Preferences | Editor | Inspections" : unter "Quality Tools" Configure "PHP Code Sniffer validation"

If the tool is activated once, you get a lot of instructions when editing the PHP files.


### TypoScript Linter ###

See https://github.com/martin-helmich/typo3-typoscript-lint

The TYPO3 TypoScript linter is installed via composer's dev-dependencies. Again, it's advisable to install the tool
globally on your local development machine.

Executing the linter is quite easy:

`./vendor/bin/typoscript-lint -c ./typoscript-lint.yml`

The file `typoscript-lint.yml` contains the configuration for the linter. Feel free to suggest improvements.

**Note:** The linter only suggests changes. It won't help you with fixing the throughout your TS folders. But your
IDE should help you at least with the indentation (via `.editorconfig`).

### SCSS Linter ###

See

* https://stylelint.io/ and https://github.com/stylelint/stylelint
* https://github.com/stylelint/stylelint-config-standard
* https://github.com/kristerkari/stylelint-scss

The configuration of stylelint is to be found in `.stylelintrc.yml`.

Execute the scss linter via:

`npm run test:scss`

Or run it natively against specific directories or files via:

`./node_modules/.bin/stylelint ./public/assets/src/scss/modules/` (or similar)

To **automagically** fix most of the indentation and other well catchable code formatting issues,
use the `--fix` option, for example:

`./node_modules/.bin/stylelint --fix ./public/assets/src/scss/modules/_hero.scss`

The shortcut for running the fixing task would be:

`npm run fix:scss`

**! Notice:** SCSS Linter is called before every `git commit ...` command, to skip checking SCSS code run `git commit ... --no-verify` (not recommended).

## Deployment script ##

A basic deployment script based on [PHP Deployer](https://deployer.org/) is included in this boilerplate.

For more information refer to the CAG dev team and / or read the [Deployment Article](https://connecta-ag.atlassian.net/wiki/spaces/OFFICE/pages/542572545/Einrichtung+Autodeployment+Jenkins+Bitbucket)
in CAG Confluence Wiki.

## To Do / Planned Features ##

* Add more linting / code quality tools and checks
  * JS
  * PHP Mess Detector, etc.
* Make linting tools execute upon commits or pull requests
* find out if separate webpack compile jobs for CSS and JS are possible
* add option to include only parts of Foundation instead of whole Foundation JS
* check, if a JS source map is needed / easy to implement as well
* improve development / production context separation: https://webpack.js.org/guides/production/
* set up linting in webpack.config.js
* include image processing in webpack???

**Note:** Please, send comments and proposals for improvements to j.rieger@connecta.ag

## Who do I talk to? ###

* Jochen Rieger
* Matthias Krams
* Andreas Buecking

Connecta AG
+49 611 3 41 09 0
