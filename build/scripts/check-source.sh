#!/bin/bash

BOLD='\033[1m'
RESET='\033[0m'

if [ -z "$1" ]; then
    echo -e "\nPlease call ${BOLD}$0 <path_to_tar>${RESET} to run this command!\n"
    exit 1
fi

TEMP_PATH="_temp"

rm -rf ./$TEMP_PATH/target && mkdir -p ./$TEMP_PATH/target
tar -xzf "$1" -C ./$TEMP_PATH/target --no-same-permissions
find "$TEMP_PATH/target" -type f | sort -k 2 | sed -e's/_temp\/target\///g' > "$TEMP_PATH/list.txt"
while read line;
do
  diff -q "$TEMP_PATH/target/$line" "$line"
done < "$TEMP_PATH/list.txt"
rm -rf ./$TEMP_PATH/target && rm ./$TEMP_PATH/list.txt
