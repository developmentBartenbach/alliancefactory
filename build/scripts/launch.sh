#!/bin/bash

source $(dirname $0)/common.sh

case $OSTYPE in
  linux-gnu)
    xdg-open $url
    ;;
  "darwin"*)
    open $url
    ;;
  "win*"* | "msys"*)
    start $url
    ;;
esac
