#!/bin/bash

source $(dirname $0)/common.sh

echo -e "\n${BOLD}TYPO3 Project Information${RESET}\n"
echo -e "  ${BOLD}Project name${RESET}        -  $projectname"
echo -e "  ${BOLD}Project repository${RESET}  -  $projectrepo"
echo -e "  ${BOLD}PHP version${RESET}         -  $phpversion"
echo -e "  ${BOLD}MySQL version${RESET}       -  $mysqlversion\n"
echo -e "  ${BOLD}MySQL user${RESET}          -  $mysqluser"
echo -e "  ${BOLD}MySQL password${RESET}      -  $mysqlpass"
echo -e "  ${BOLD}MySQL database${RESET}      -  $mysqldb\n"
echo -e "  ${BOLD}Connect to mysql from your host machine${RESET}   -   mysql --host=127.0.0.1 --port=$mysqlport --user=$mysqluser --password=$mysqlpass --database=$mysqldb"
echo -e "  ${BOLD}Connect to mysql from docker container${RESET}    -   mysql --host=database --user=$mysqluser --password=$mysqlpass --database=$mysqldb\n"
echo -e "  ${BOLD}Homepage${RESET}            -  https://$domain/"
echo -e "  ${BOLD}Backend${RESET}             -  https://$domain/typo3"
echo -e "  ${BOLD}Adminer${RESET}             -  http://localhost:8080"
echo -e "  ${BOLD}MailHog${RESET}             -  http://localhost:8025\n"
