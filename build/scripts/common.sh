#!/bin/bash

BOLD='\033[1m'
RESET='\033[0m'

while [ $# -gt 0 ]; do
   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
   fi
  shift
done
