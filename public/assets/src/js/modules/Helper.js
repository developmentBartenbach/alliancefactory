/**
 * Detect IE
 *
 * @return {*}
 */
export function detectIE() {
  let ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  let msie = ua.indexOf("MSIE ");
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
  }

  let trident = ua.indexOf("Trident/");
  if (trident > 0) {
    // IE 11 => return version number
    let rv = ua.indexOf("rv:");
    return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
  }

  let edge = ua.indexOf("Edge/");
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
  }

  // other browser
  return false;
}

/**
 * resize dots map to videos size
 *
 * @return {*}
 */
export function resizeMap(mymap, myvideo, duration) {
  let video = document.querySelector(myvideo);
  // check it if first video is hide
  if( video.offsetWidth == 0 ) {
    video = document.querySelectorAll(myvideo)[1];
  }

  // calculate size from video (4:3)
  setTimeout(() => {
    document.querySelector(mymap).style.width = `${
      video.offsetWidth
    }px`;
    document.querySelector(mymap).style.height = `${
      ( video.offsetWidth / 4 ) * 3
    }px`;
  }, duration);
}

export function activeDotsPosition() {
  const dots = document.querySelectorAll('.ce-dot');
  if( dots.length > 0 ){
    for(let i = 0; i < dots.length; i++) {
      dots[i].style.top = `${dots[i].dataset.top}%`;
      dots[i].style.left = `${dots[i].dataset.left}%`;
    }
  }

}

export function dotsRandomDelays() {
  const dots = document.querySelectorAll('.ce-dot'),
    initDelay = 50;
    let arrayDelays = [];

  if( dots.length > 0 ){
    for(let i = 0; i < dots.length; i++) {
      arrayDelays.push( initDelay * (i + 1) );
    }
    // disable random array
    arrayDelays.sort( (a, b) => {
      return 0.5 - Math.random()
    });

    for(let i = 0; i < dots.length; i++) {
      dots[i].setAttribute('data-aos-delay',  arrayDelays[i]);
      dots[i].firstElementChild.setAttribute('data-aos-delay',  arrayDelays[i] + 200 );
    }
  }
}
