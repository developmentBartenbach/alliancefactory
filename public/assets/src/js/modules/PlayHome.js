'use strict';

import LazyLoad from "vanilla-lazyload";
import AOS from "aos";

/**
 * Just a basic example class to show that the ES6 transpiling works
 *
 */

// ES6 class definition
export default class PlayHome {

  constructor() {
    this.whatAmI = `I'm an ES6 class playHome`;
    this.playButton = document.querySelector('.ce-hero__play');
    this.heroText = document.querySelector('.ce-hero__text');
    this.heroCards = document.querySelector('.ce-hero__cards');
    this.playVideo = document.querySelector('.lazy-video-start');
    this.autoPlayLoop = document.querySelector('.lazy-video-loop');
    this.disableAos = document.querySelectorAll('.aos-out');
    this.activeAos = document.querySelectorAll('.aos-in');
    this.durationOut = 200;
    this.debugMode = false;
  }

  listenPlay() {
    let self = this;

    if( self.playButton ) {
      self.playButton.addEventListener('click', e => {
        e.preventDefault();

        self.playButton.setAttribute('disabled', true); // disable play to click only once

        self.outAnimate(); // animate text and card to out

        self.inAnimate(); // animate dots to in

        // play intro video anche check ended to remove it
        let promise = self.playVideo.play();

        if (promise !== undefined) {
          promise.then(_ => {
            // Autoplay started!
            self.playVideo.addEventListener('ended', () => {
              self.autoPlayLoop.play(); // play infinite video
              self.disableContent(); // disable text content and cards
            });
          }).catch(error => {
            // Autoplay was prevented.
            // Show a "Play" button so that user can start playback.
            self.logToConsole(`Error play ${error}`);
          });
        }
      });
    }

  }

  /**
   * Method to log objects, strings, etc. to console; only acts if debugMode is on
   *
   */
  outAnimate() {
    let self = this;
    // disable some element with a out animate (text and card)
    for(let i = 0; i < self.disableAos.length; i ++) {
      setTimeout( ()=> {
        self.disableAos[i].dataset.aos = self.disableAos[i].dataset.outaos;
        self.disableAos[i].classList.remove('aos-animate');
      }, (self.durationOut * (i+7)));
    }

    // active some animate in header
    for(let i = 0; i < self.activeAos.length; i ++) {
      setTimeout( ()=> {
        self.activeAos[i].classList.remove('aos-animate');
        self.activeAos[i].classList.remove('aos-in');
      }, (self.durationOut * i));
    }
  }

  /**
   * Method to log objects, strings, etc. to console; only acts if debugMode is on
   *
   */
  inAnimate(){
    let self = this;
    // active some element with in animate, headers link and dots
      setTimeout( ()=> {
        for(let i = 0; i < self.activeAos.length; i ++) {
          self.activeAos[i].classList.add('aos-animate');
        }
      }, (self.durationOut * (self.disableAos.length + 11 ))); // begin to outAnimate, sychronize via time
  }

  /**
   * Method to log objects, strings, etc. to console; only acts if debugMode is on
   *
   */
  disableContent(){
    let self = this;
    self.playVideo.style.display = 'none';
    self.heroText.style.display = 'none';
    self.heroCards.style.display = 'none';
  }

  /**
   * Method to log objects, strings, etc. to console; only acts if debugMode is on
   * @param consoleOutput
   */
  logToConsole(consoleOutput) {
    let self = this;

    if (self.debugMode) {
      console.log(consoleOutput);
    }
  }

}
