'use strict';

/**
 * Class to control some event in popups
 *
 */

// ES6 class definition
export default class PopupEvent {

  constructor() {
    this.whatAmI = `I'm an ES6 class PopupEvent`;
    this.mainPopup = document.querySelectorAll('.ce-popup');
    this.mainColumn = document.querySelectorAll('.ce-popup__main');
    this.middleColumn = document.querySelectorAll('.ce-popup__middle');
    this.endColumn = document.querySelectorAll('.ce-popup__end');
    this.level1 = document.querySelectorAll('.ce-popup__lvl-1');
    this.level2 = document.querySelectorAll('.ce-popup__lvl-2');
    this.item_middle = document.querySelectorAll('.ce-popup__item-middle');
    this.item_end = document.querySelectorAll('.ce-popup__item-end');
    this.button_back = document.querySelectorAll('.ce-popup__back');
    this.button_close = document.querySelectorAll('.ce-popup__close');
    this.delay_to_close = 800;
    this.debugMode = false;
  }

  init() {
    let self = this;
    self.eventListenerMainPopup()
    self.eventListenerFirstLevel();
    self.eventListenerSecondLevel();
    self.eventListenerBack();
    self.eventListenerClosePopup();
    self.logToConsole(`Init Class PopupEvent`);
  }

  /**
   * Method to listen click event in main content and columns
   * @param
   */
  eventListenerMainPopup(){
    let self = this;
    if( self.mainPopup ) {
      for( let i = 0; i < self.mainPopup.length; i++ ) {
        self.mainPopup[i].addEventListener( 'click', e => {
          self.mainPopup[i].lastElementChild.click();
        });

        // stpo propagation event click
        self.stopClickPropagation(self.mainColumn[i]);
        self.stopClickPropagation(self.middleColumn[i]);
        self.stopClickPropagation(self.endColumn[i]);
      }
    }
  }

  stopClickPropagation( element ){

    element.addEventListener( 'click', e => {
      e.stopPropagation();
    });
  }

  /**
   * Method to close current popup
   * @param
   */
  eventListenerClosePopup(){
    let self = this;
    if( self.button_close ) {
      for( let i = 0; i < self.button_close.length; i++ ) {

        self.button_close[i].addEventListener( 'click', e => {
          const popup = document.getElementById(window.location.hash.split('#')[1]);

          if(!self.button_close[i].classList.contains('closing')) {
            e.preventDefault();
            e.stopPropagation();
            self.closeAllLevels();

            setTimeout( () => {
              self.button_close[i].classList.add('closing');
              popup.classList.add('closing');
            }, popup.classList.contains('active-lvl-1') ? 0 : self.delay_to_close/2 );

            setTimeout( () => {
              self.button_close[i].click();
            }, popup.classList.contains('active-lvl-1') ? self.delay_to_close/4 : self.delay_to_close);

            self.logToConsole(`Closed popup ${window.location.hash}`);
          }
          else {
            popup.classList.remove('closing');
            self.button_close[i].classList.remove('closing');
          }

        });
      }
    }
  }

  /**
   * Method to to listen slideout arrow button in second column
   * @param
   */
  eventListenerBack() {
    let self = this;
    if (self.button_back.length) {
      for (let i = 0; i < self.button_back.length; i++) {
        self.button_back[i].addEventListener('click', e => {
          e.preventDefault();
          self.closeThirdLevel();
          self.logToConsole(`Back to second column`);
        });
      }
    }
  }

  /**
   * Method to to listen menus links in first column to open or close the second column
   * @param
   */
  eventListenerFirstLevel() {
    let self = this;

    if (self.level1.length) {

      for (let i = 0; i < self.level1.length; i++) {
        self.level1[i].addEventListener('click', e => {
          e.preventDefault();
          const hashLvl1 = self.level1[i].hash,
           lvl2 = document.getElementById(hashLvl1.split('#')[1]),
           hashpopup = window.location.hash;

          if (self.level1[i].classList.contains('active')) {
            self.closeAllLevels();
            self.logToConsole(`Click link ${hashLvl1} to close second column in ${hashpopup}`);
          } else if( lvl2 ) {
            self.closeAllLevels();
            self.activeNextLevel( self.level1[i], lvl2, 1 );
            self.logToConsole(`Click link ${hashLvl1} to open second column in ${hashpopup}`);
          }
          else {
            self.closeAllLevels();
            self.logToConsole(`Click link ${hashLvl1} to close all column in ${hashpopup}, no found content`);
          }

        });
      }
    }
  }

  /**
   * Method to to listen menus links in second column to open or close the third column
   * @param
   */
  eventListenerSecondLevel() {
    let self = this;

    if (self.level2.length) {

      for (let i = 0; i < self.level2.length; i++) {
        self.level2[i].addEventListener('click', e => {
          e.preventDefault();
          const hashLvl2 = self.level2[i].hash,
            lvl3 = document.getElementById(hashLvl2.split('#')[1]),
            hashpopup = window.location.hash;

          if (self.level2[i].classList.contains('active')) {
            self.closeThirdLevel();
            self.logToConsole(`Click link ${hashLvl2} to close third column in ${hashpopup}`);
          } else if( lvl3 ) {
            self.closeThirdLevel();
            self.activeNextLevel( self.level2[i], lvl3, 2 );
            self.logToConsole(`Click link ${hashLvl2} to open third column in ${hashpopup}`);
          }else {
            self.logToConsole(`Click link ${hashLvl2} in ${hashpopup}, no found content`);
          }
        });
      }
    }
  }

  /**
   * Method to close second and third columns
   * @param
   */
  closeAllLevels() {
    let self = this;
    // close the third column
    self.closeThirdLevel();

    // close second column
    self.closeSecondLevel();

    self.logToConsole(`Call method 'closeAllLevels' to close all column`);
  }

  /**
   * Method to close third column
   * @param
   */
  closeThirdLevel(){
    let self = this;
    const popup = document.getElementById(window.location.hash.split('#')[1]);
    // close the third column
    self.removeClass(self.level2, 'active');
    popup.classList.remove('active-lvl-2');
    self.removeClass(self.item_end, 'active');
    self.logToConsole(`Call method 'closeThirdLevel' to close third column`);
  }

  /**
   * Method to close second column
   * @param
   */
  closeSecondLevel() {
    let self = this;
    const popup = document.getElementById(window.location.hash.split('#')[1]);

    popup.classList.remove('active-lvl-1');
    self.removeClass(self.level1, 'active');
    self.removeClass(self.item_middle, 'active');
    self.logToConsole(`Call method 'closeSecondLevel' to close second column`);
  }

  /**
   * Method to remove some classes css in different items
   * @param
   */
  removeClass(items, className) {
    let self = this;

    for (let i = 0; i < items.length; i++) {
      if (items[i].classList.contains(className)) items[i].classList.remove(className);
    };

  }

  /**
   * Method to open next column
   * @param
   */
  activeNextLevel( currentLvl, nextLvl, lvl) {
    let self = this;
    const popup = document.getElementById(window.location.hash.split('#')[1]);

    currentLvl.classList.add('active');
    popup.classList.add(`active-lvl-${lvl}`);
    nextLvl.classList.add('active');

    self.logToConsole(`Call method to open ${lvl} column`);
  }

  /**
   * Method to log objects, strings, etc. to console; only acts if debugMode is on
   * @param consoleOutput
   */
  logToConsole(consoleOutput) {
    let self = this;

    if (self.debugMode) {
      console.log(consoleOutput);
    }
  }

}
