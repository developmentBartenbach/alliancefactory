'use strict';

// to check if main.js is loaded in the browser at all
// console.info('Let the games begin - Hello main.js! :)');

// import some (vendor) libs
import viewportUnitsBuggyfill from "viewport-units-buggyfill";
import LazyLoad from "vanilla-lazyload";
import AOS from "aos";
import Simplebar  from 'simplebar';

// import / include the class definition
import playHome from './modules/PlayHome';
import popupEvent from './modules/PopupEvent';
import {
  dotsRandomDelays,
  detectIE,
  resizeMap,
  activeDotsPosition
} from './modules/Helper';


let myplayHome = new playHome();
let mypopupEvent = new popupEvent();

/**
 *
 * Let the games begin! This is the very entry point of our app / website js.
 *
 *
 */

document.addEventListener("DOMContentLoaded", function () {

  // active different lazyloading to videos and images
  try {
    // init lazyloading videos
    let lazyLoadInstance = new LazyLoad({
      elements_selector: ".lazy-video-start",
      load_delay: 500, //adjust according to use case
      callback_loaded: (el) => {
        // active animate with delay
        setTimeout(() => {
          try {
            // init AOS Animate on scroll library
            AOS.init({
              duration: 700, // values from 0 to 1500, with step 100ms
              offset: (document.querySelector('video').offsetWidth) ? -(document.querySelector('video').offsetWidth / 4) * 3 : 200,
              easing: "ease-out-quart", // default easing for AOS animations
              once: true, // whether animation should happen only once - while scrolling down
              anchorPlacement: "top-bottom", // defines which position of the element regarding to window should trigger the animation
            });
          } catch (type_error) {
            print_error_message("AOS", type_error);
          }
        }, 500);
      },
      // ... more custom settings?
    });
  } catch (type_error) {
    print_error_message("Lazy", type_error);
  }

  try {
    // init lazyloading infinite video
    new LazyLoad({
      elements_selector: ".lazy-video-loop",
      load_delay: 4000, //adjust according to use case
      // ... more custom settings?
    });

    // init lazyloading images in popup
    new LazyLoad({
      elements_selector: ".lazy-image",
      load_delay: 500, //adjust according to use case
      // ... more custom settings?
    });

  } catch (type_error) {
    print_error_message("Lazy", type_error);
  }

  // class to control home init
  try {
    myplayHome.listenPlay();
  } catch (type_error) {
    print_error_message("Button play", type_error);
  }

  // page full screen to every devices (viewport units)
  try {
    // ignore user agent force initialization
    // active only on tablet and mobile
    viewportUnitsBuggyfill.init({
      force: true
    });
  } catch (type_error) {
    print_error_message("ViewportUnitsBuggyfill", type_error);
  }

  // create random delays for dots animate and update its position
  try {
    // calculate size from size
    resizeMap('.ce-hero__map', 'video', 1000);
    dotsRandomDelays();
    activeDotsPosition();
    window.addEventListener('resize', () => {
      resizeMap('.ce-hero__map', 'video', 500);
      activeDotsPosition();
    });
  } catch (type_error) {
    print_error_message("Helper Resize function", type_error);
  }

  // class to listen differents events for popups
  try {
    mypopupEvent.init();
  } catch (type_error) {
    print_error_message("Popup event", type_error);
  }

  // active modus ie, special css
  try {
    if (detectIE()) {
      document.body.classList.add('active-ie');
    }
  } catch (type_error) {
    print_error_message("Helper detect IE", type_error);
  }

  // button to close and open tooltip
  try {
    const dots = document.querySelectorAll('.ce-dot'),
      buttons_tooltip = document.querySelector('.page-footer__link');

    buttons_tooltip.addEventListener('click', e => {
      e.preventDefault();
      buttons_tooltip.classList.toggle('change');
      for (let i = 0; i < dots.length; i++) {

        if (dots[i].firstElementChild.classList.contains('aos-animate')) {
          // animate open
          dots[i].firstElementChild.setAttribute('data-aos', 'zoom-out');
          // add a delay to open
          setTimeout(() => {
            dots[i].firstElementChild.classList.remove('aos-animate');
          }, dots[i].firstElementChild.getAttribute('data-aos-delay'));

        } else {
          // animate close
          dots[i].firstElementChild.setAttribute('data-aos', 'zoom-in');
          dots[i].firstElementChild.classList.add('aos-animate');
        }

      }
    });
  } catch (type_error) {
    print_error_message("Footer Button", type_error);
  }

});

function print_error_message(type_error, original_error) {
  console.error("Code error: " + type_error);
  console.error(original_error.message);
}
